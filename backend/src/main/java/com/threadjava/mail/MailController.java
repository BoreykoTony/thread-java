package com.threadjava.mail;

import com.threadjava.post.PostsService;
import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.UUID;

@Service
public class MailController {

    @Autowired
    public JavaMailSender emailSender;
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private PostsService postsService;

    public boolean sendHtmlEmail(UUID postId, UUID userId) throws MessagingException {
        UserDetailsDto from = userDetailsService.getUserById(userId);
        PostDetailsDto post = postsService.getPostById(postId);
        // FIXME: fix up this s**t
        UserDetailsDto to = userDetailsService.getUserById(post.getUser().getId());

        String url = "http://192.168.0.100:3000/share/"+post.getId();

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        String htmlMsg = String.format("<p>Hello, <strong>%s</strong>!</p>" +
                "<p>Your post has been liked by <strong>%s</strong>.</p>"
                +"<p>Look this post: <a href=\"%s\">link</a></p>",
                to.getUsername(), from.getUsername(), url);
        message.setContent(htmlMsg, "text/html");
        helper.setTo(to.getEmail());
        helper.setSubject("Post reaction!");
        this.emailSender.send(message);
        return true;
    }
}
