package com.threadjava.comment;

import com.threadjava.comment.dto.CommentListQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.dto.PostDetailsQueryResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "p.user) " +
            "FROM Comment p " +
            "WHERE p.post.id = :postId AND p.isActive = TRUE")
    List<CommentListQueryResult> findAllByPostId(@Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "p.user) " +
            "FROM Comment p " +
            "WHERE p.id = :id AND p.isActive = TRUE")
    Optional<CommentListQueryResult> findCommentById(@Param("id") UUID id);

    @Modifying
    @Transactional
    @Query("UPDATE Comment p " +
            "SET p.isActive = FALSE " +
            "WHERE p.id = :id")
    void deleteCommentById(@Param("id") UUID id);
}