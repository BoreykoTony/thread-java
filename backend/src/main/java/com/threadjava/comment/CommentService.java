package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDeletionDto;
import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.post.PostMapper;
import com.threadjava.post.PostsRepository;
import com.threadjava.post.dto.PostDeletionResponseDto;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public CommentDeletionDto deleteCommentById(UUID id) {
        var comment = getCommentById(id);
        if (comment.getUser().getId().equals(getUserId())){
            commentRepository.deleteCommentById(id);
            return CommentMapper.MAPPER.commentToCommentDeletionDto(comment);
        }else{
            return null;
        }
    }
}
