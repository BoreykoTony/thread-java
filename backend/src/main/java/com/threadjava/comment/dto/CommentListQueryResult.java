package com.threadjava.comment.dto;

import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentListQueryResult {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private User user;
}
