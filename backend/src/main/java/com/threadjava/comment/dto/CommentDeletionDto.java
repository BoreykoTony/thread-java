package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentDeletionDto {
    private UUID id;
    private UUID userId;
}
