package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionsRepository.getPostReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                var result = commentReactionsRepository.save(react);
                result.setHasChanged(true);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToPostReactionDto(result));
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToPostReaction(commentReactionDto);
            var result = commentReactionsRepository.save(commentReaction);
            return Optional.of(CommentReactionMapper.MAPPER.reactionToPostReactionDto(result));
        }
    }
}
