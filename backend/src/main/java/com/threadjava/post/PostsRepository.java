package com.threadjava.post;

import com.threadjava.image.model.Image;
import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.model.Post;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("SELECT DISTINCT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isActive = TRUE THEN 1 ELSE 0 END), 0) FROM p.comments pr WHERE pr.post = p), " +
            "p.createdAt, i, p.user) " +
            "FROM Post p " +
            "INNER JOIN p.reactions r " +
            "LEFT JOIN p.image i " +
            "WHERE (:option = 0 OR (:option = 1 AND p.user.id = :userId) OR (:option = -1 AND p.user.id != :userId)) AND p.isActive = TRUE " +
            "AND (:liked = FALSE OR (r.user.id = :userId AND r.isLike = TRUE)) " +
            "order by p.createdAt desc" )
    List<PostListQueryResult> findAllPosts(@Param("userId") UUID userId, @Param("option") Integer option, @Param("liked") Boolean liked, Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isActive = TRUE THEN 1 ELSE 0 END), 0) FROM p.comments pr WHERE pr.post = p), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id AND p.isActive = TRUE ")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);

    @Modifying
    @Transactional
    @Query("UPDATE Post p " +
            "SET p.isActive = FALSE " +
            "WHERE p.id = :id")
    void deletePostById(@Param("id") UUID id);

    @Modifying
    @Transactional
    @Query("UPDATE Post p " +
            "SET p.body = :body, p.image = :image " +
            "WHERE p.id = :id ")
    void updateById(@Param("id") UUID id, @Param("body") String body, @Param("image") Image image);
}