package com.threadjava.users;

import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmail(String email);

    @Query("SELECT 1 FROM User u WHERE u.username = :username ")
    Optional<User> existsByUsername(@Param("username") String username);

    @Modifying
    @Transactional
    @Query("UPDATE User u " +
            "SET u.username = :username " +
            "WHERE u.id = :id")
    void setUsernameById(@Param("username") String username, @Param("id") UUID id);

    @Modifying
    @Transactional
    @Query("UPDATE User u " +
            "SET u.avatar.id = :img_id " +
            "WHERE u.id = :id")
    void setAvatar(UUID img_id, UUID id);
    //TODO: add RETURNING
}