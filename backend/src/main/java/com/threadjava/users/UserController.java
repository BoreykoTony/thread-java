package com.threadjava.users;

import com.threadjava.image.dto.ImageDto;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PostMapping("/setuser")
    public UserDetailsDto setUsername(@RequestBody String username) {
        return userDetailsService.setUsername(username, getUserId());
    }

    @PostMapping("/setavatar")
    public UserDetailsDto setUsername(@RequestBody ImageDto imageDto) {
        return userDetailsService.setAvatar(imageDto, getUserId());
    }
}
