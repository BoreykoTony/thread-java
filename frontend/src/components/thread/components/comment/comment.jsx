import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

import { Card, Label } from '../../../common/common';
import { IconName } from '../../../../common/enums/components/icon-name.enum';

const Comment = ({ comment, onCommentLike, onCommentDislike, onCommentDelete }) => {
  const {
    id,
    body,
    createdAt,
    likeCount,
    dislikeCount,
    user
  } = comment;

  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const handleCommentDelete = () => onCommentDelete(id);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
        <Card.Content extra>
          <Label
            basic
            size="small"
            as="a"
            onClick={handleCommentLike}
          >
            <Icon name={IconName.THUMBS_UP} />
            {likeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            onClick={handleCommentDislike}
          >
            <Icon name={IconName.THUMBS_DOWN} />
            {dislikeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
          >
            <Icon name={IconName.EDIT} />
          </Label>
          <Label
            basic
            size="small"
            as="a"
            onClick={handleCommentDelete}
            className={styles.toolbarBtn}
          >
            <Icon name={IconName.DELETE} />
          </Label>
        </Card.Content>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;
