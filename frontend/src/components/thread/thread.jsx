import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  option: 0,
  liked: false,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [hideOwnPosts, setHideOwnPosts] = React.useState(false);
  const [likedByMe, setLikedByMe] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.reactPost(id, true))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.reactPost(id, false))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handlePostEdit = React.useCallback(post => {
    dispatch(threadActionCreator.editPost(post));
  }, [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const setPostsViewOption = (option, liked) => {
    postsFilter.userId = showOwnPosts && hideOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.liked = liked;
    postsFilter.option = option;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setHideOwnPosts(showOwnPosts && hideOwnPosts);
    setPostsViewOption(Number(!showOwnPosts), likedByMe);
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    setShowOwnPosts(hideOwnPosts && showOwnPosts);
    setPostsViewOption((-1) * Number(!hideOwnPosts), likedByMe);
  };

  const toggleShowLikedPosts = () => {
    setLikedByMe(!likedByMe);
    setPostsViewOption(Number(showOwnPosts) + (-1) * Number(hideOwnPosts), !likedByMe);
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
        <Checkbox
          toggle
          label="Liked by me"
          checked={likedByMe}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostEdit={handlePostEdit}
            onPostDelete={handlePostDelete}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            userId={userId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
