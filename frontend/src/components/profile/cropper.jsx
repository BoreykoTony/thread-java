import React, { useState, useCallback, useRef, useEffect } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import PropTypes from 'prop-types';
import { Button } from '../common/common';
import { IconName } from '../../common/enums/components/icon-name.enum';
import styles from '../thread/components/add-post/styles.module.scss';

const Cropper = ({ onSubmitted }) => {
  const [upImg, setUpImg] = useState();
  const downImg = useRef(null);
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({ unit: 'px', width: 200, aspect: 1 });
  const [completedCrop, setCompletedCrop] = useState(null);

  function convertToImg(canvas, cropped, resolve) {
    if (!cropped || !canvas) {
      return;
    }
    canvas.toBlob(
      blob => {
        downImg.current = URL.createObjectURL(blob);
        resolve(blob);
      },
      'image/png'
    );
  }

  const handleSubmitted = () => convertToImg(previewCanvasRef.current, completedCrop, onSubmitted);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }
    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const cropped = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = cropped.width * pixelRatio;
    canvas.height = cropped.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      cropped.x * scaleX,
      cropped.y * scaleY,
      cropped.width * scaleX,
      cropped.height * scaleY,
      0,
      0,
      cropped.width,
      cropped.height
    );
  }, [completedCrop]);

  return (
    <div className="Cropper">
      {upImg && (
        <canvas
          ref={previewCanvasRef}
        />
      )}
      <br />
      <Button
        color="teal"
        iconName={IconName.IMAGE}
      >
        <label className={styles.btnImgLabel}>
          Attach image
          <input
            name="image"
            type="file"
            onChange={onSelectFile}
            hidden
          />
        </label>
      </Button>
      <ReactCrop
        src={upImg}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={c => setCrop(c)}
        onComplete={c => setCompletedCrop(c)}
        minWidth={50}
        maxWidth={200}
      />
      <br />
      {upImg && (
        <Button
          color="teal"
          iconName={IconName.IMAGE}
          onClick={handleSubmitted}
        >
          <label className={styles.btnImgLabel}>
            Upload image
          </label>
        </Button>
      )}
    </div>
  );
};

Cropper.propTypes = {
  onSubmitted: PropTypes.func.isRequired
};

export default Cropper;
