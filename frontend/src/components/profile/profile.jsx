import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Grid, Image, Input } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { profileActionCreator } from 'src/store/actions';
import { image as imageService } from '../../services/services';
import { Form } from '../common/common';
import { ButtonType } from '../../common/enums/components/button-type.enum';
import { ButtonColor } from '../../common/enums/components/button-color.enum';
import { ButtonSize } from '../../common/enums/components/button-size.enum';
import Cropper from './cropper';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [login, setLogin] = React.useState('');
  const dispatch = useDispatch();

  const setAvatar = React.useCallback((id, link) => (
    dispatch(profileActionCreator.setAvatar(id, link))
  ), [dispatch]);

  const setUsername = React.useCallback(username => (
    dispatch(profileActionCreator.setUsername(username))
  ), [dispatch]);

  const handleUploadFile = target => {
    const file = target;
    imageService.uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setAvatar(imageId, imageLink);
      })
      .catch(() => {
      });
  };

  const handleChangeUsername = () => {
    setUsername(login);
  };

  const loginChanged = data => {
    setLogin(data);
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image
          centered
          src={user.image?.link ?? DEFAULT_USER_AVATAR}
          size="medium"
          circular
        />
        <br />
        <Cropper
          onSubmitted={handleUploadFile}
        />
        <br />
        <br />
        <Form.Input
          icon="user"
          iconPosition="left"
          placeholder={user.username}
          type="text"
          onChange={ev => loginChanged(ev.target.value)}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Button
          type={ButtonType.SUBMIT}
          color={ButtonColor.TEAL}
          size={ButtonSize.LARGE}
          onClick={handleChangeUsername}
          isPrimary
        >
          Update
        </Button>
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
