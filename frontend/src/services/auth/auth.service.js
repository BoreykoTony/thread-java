import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  login(payload) {
    return this._http.load('/api/auth/login', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load('/api/auth/register', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  getCurrentUser() {
    return this._http.load('/api/user', {
      method: HttpMethod.GET
    });
  }

  setUsername(nickname) {
    return this._http.load('/api/user/setuser', {
      method: HttpMethod.POST,
      payload: nickname
    });
  }

  setAvatar(id, link) {
    return this._http.load('/api/user/setavatar', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        id,
        link
      })
    });
  }
}

export { Auth };
