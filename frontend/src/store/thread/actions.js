import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  DELETE_POST: 'thread/delete-post',
  SET_COMMENTS: 'thread/set-comments'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const setComments = createAction(ActionType.SET_COMMENTS, comments => ({
  payload: {
    comments
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const removePost = createAction(ActionType.DELETE_POST, id => ({
  payload: {
    id
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const deletePost = postId => async dispatch => {
  const { id } = await postService.deletePost(postId);
  dispatch(removePost(id));
};

const editPost = post => async () => {
  const { id } = await postService.editPost(post);
  applyPost(id);
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id } = await commentService.deleteComment(commentId);
  if (id !== null) {
    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: post.comments.filter(comment => (comment.id !== commentId))
    });
    const mapNumOfComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) - 1
    });
    const {
      posts: { posts, expandedPost }
    } = getRootState();

    const updated = posts.map(post => (post.id !== expandedPost.id ? post : mapNumOfComments(post)));
    dispatch(setPosts(updated));

    if (expandedPost) {
      dispatch(setExpandedPost(mapComments(expandedPost)));
    }
  }
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const reactPost = (postId, isLike) => async (dispatch, getRootState) => {
  const response = await postService.reactPost(postId, isLike);
  let [diff1, diff2] = response?.id ? [0, 1] : [0, -1];
  if (diff2 === 1 && response.hasChanged) {
    [diff1, diff2] = [-1, 1];
  }
  if (isLike) {
    [diff1, diff2] = [diff2, diff1];
  }
  const mapReactions = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff1,
    dislikeCount: Number(post.dislikeCount) + diff2
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReactions(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapReactions(expandedPost)));
  }
};

const reactComment = (commentId, isLike) => async (dispatch, getRootState) => {
  const response = await commentService.reactComment(commentId, isLike);
  let [diff1, diff2] = response?.id ? [0, 1] : [0, -1];
  if (diff2 === 1 && response.hasChanged) {
    [diff1, diff2] = [-1, 1];
  }
  if (isLike) {
    [diff1, diff2] = [diff2, diff1];
  }
  const mapReactions = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff1,
    dislikeCount: Number(comment.dislikeCount) + diff2
  });

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id !== commentId ? comment : mapReactions(comment)))
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  removePost,
  setComments,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  deletePost,
  editPost,
  deleteComment,
  toggleExpandedPost,
  reactPost,
  reactComment,
  addComment
};
